import time

import pytest
from selenium.webdriver import Chrome

from pages.admin_page import ArenaAdminPage
from pages.home_page import ArenaHomePage
from pages.login_page import ArenaLoginPage
from pages.project_page import ArenaProjectPage


@pytest.fixture
def browser():
    driver = Chrome()
    yield driver
    time.sleep(10)
    driver.quit()


def test_project(browser):
    browser.get('http://demo.testarena.pl/zaloguj')
    login_page = ArenaLoginPage(browser)
    login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    home_page = ArenaHomePage(browser)
    home_page.click_panel_admin('Projekty')
    admin_page = ArenaAdminPage(browser)
    admin_page.click_button_new_project('Dodaj projekt')
    project_page = ArenaProjectPage(browser)
    project_page.click_input_name()
    text = project_page.send_keys_random_text_name()
    project_page.send_keys_random_text_prefix()
    project_page.send_keys_random_text_description()
    project_page.click_send()
    project_page.click_section_project()
    admin_page.search_added_new_project(text)
