from selenium.webdriver.common.by import By
from utils.random_text_project import generata_random_text


class ArenaProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def click_input_name(self):
        self.browser.find_element(By.CSS_SELECTOR, '#name').click()

    def send_keys_random_text_name(self):
        random_text_name = generata_random_text(10)
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_text_name)
        return random_text_name

    def send_keys_random_text_prefix(self):
        random_text_prefix = generata_random_text(5)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_text_prefix)

    def send_keys_random_text_description(self):
        random_text_description = generata_random_text(50)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_text_description)

    def click_send(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()
        assert self.browser.find_element(By.CSS_SELECTOR, '#j_info_box')

    def click_section_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_puzzle_alt').click()
