from selenium.webdriver.common.by import By


class ArenaHomePage:
    def __init__(self, browser):
        self.browser = browser

    def click_panel_admin(self, tilte):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == tilte
