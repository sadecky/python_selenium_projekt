from selenium.webdriver.common.by import By


class ArenaAdminPage:
    def __init__(self, browser):
        self.browser = browser

    def click_button_new_project(self, title):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link[href="http://demo.testarena.pl/administration/add_project"]').click()
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def search_added_new_project(self, text):
        results = self.browser.find_elements(By.CSS_SELECTOR, 'td a')
        list_of_project = []
        for i in results:
            list_of_project.append(i.text)
        if text in list_of_project:
            self.browser.find_element(By.LINK_TEXT, text).click()
        assert text in list_of_project
