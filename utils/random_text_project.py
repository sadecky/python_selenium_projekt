import random
import string


def generata_random_text(lenght):
    result_str = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(lenght))
    return result_str


